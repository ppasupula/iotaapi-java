package iota;


import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;


public class Main {
	static Iota iota;
	static String seed = "XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU";
    // devnet
//	static String provider = "https://nodes.devnet.iota.org:443";
//	static int minWeightMagnitude = 9;
//	static String explorerHost = "https://devnet.thetangle.org";

    // mainnet
	static String provider = "https://nodes.thetangle.org:443";

	static int minWeightMagnitude = 14;
	static String explorerHost = "https://thetangle.org";

    
    public static void main(String[] args) throws IOException, ParseException
    {

		iota = new Iota(provider, seed);
    	iota.minWeightMagnitude = minWeightMagnitude;

        String toAddres = "IETGETEQSAAJUCCKDVBBGPUNQVUFNTHNMZYUCXXBFXYOOOQOHC9PTMP9RRIMIOQRDPATHPVQXBRXIKFDDRDPQDBWTY";

        
//        mkTx(toAddres);
        

        try {
        	
        	int lastKnownAddressIndex = 1;
        	String newAdd = iota.getCurrentAddress();
        	System.out.println("newAdd:" + newAdd);
        	
        	long bal = iota.getBalance();
        	System.out.println(bal + "   ");
        	

//        	System.out.println("getAvailableAddressIndex   " + iota.getAvailableAddressIndex(null));
        	
        } catch(Throwable e) {
            System.err.println("\nERROR: Something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
        
//        makeNetworkReq();

    }
    
    public static void mkTx(String toAddres) {
    	long amountIni = 1;

        try {
			List<String> tails = iota.makeTx(toAddres, amountIni);

            System.out.println(tails);
            System.out.println("\n\n see it here " + explorerHost + "/transaction/" + tails.get(0) + " \n\n" );

        } catch(Throwable e) {
            System.err.println("\nERROR: Something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }
    

}

