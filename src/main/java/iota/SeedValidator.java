package iota;


import org.apache.commons.lang3.StringUtils;


public class SeedValidator {

    private static final int SEED_LENGTH_MIN = 41;
    private static final int SEED_LENGTH_MAX = 81;
    
    private static final String messages_invalid_characters_seed = "Your seed contains invalid characters. Only uppercase characters from A-Z and the number 9 are accepted.";
    private static final String messages_seed_to_long = "Your seed is also too long.";
    private static final String messages_seed_to_short = "Your seed is also too short.";
    private static final String messages_mixed_seed = "Your seed contains mixed case characters. Lowercase is converted to uppercase.";
    private static final String messages_to_long_seed = "Your seed should not contain more than 81 characters. Extra characters are ignored.";
    private static final String messages_to_short_seed = "Your seed does not contain enough characters. This is not secure.";
    private static final String messages_empty_seed = "Please enter a valid seed.";

    public static String isSeedValid(String seed) {
        if (!seed.matches("^[A-Z9a-z]+$")) {
            if (seed.length() > SEED_LENGTH_MAX)
                return messages_invalid_characters_seed + messages_seed_to_long;
            else if (seed.length() < SEED_LENGTH_MIN)
                return messages_invalid_characters_seed + messages_seed_to_short;
            else
                return messages_invalid_characters_seed;

        } else if (seed.matches(".*[A-Z].*") && seed.matches(".*[a-z].*")) {
            if (seed.length() > SEED_LENGTH_MAX)
                return messages_mixed_seed + messages_seed_to_long;
            else if (seed.length() < SEED_LENGTH_MIN)
                return messages_mixed_seed + messages_seed_to_short;
            else
                return messages_mixed_seed;

        } else if (seed.length() > SEED_LENGTH_MAX) {
            return messages_to_long_seed;

        } else if (seed.length() < SEED_LENGTH_MIN) {
            return messages_to_short_seed;
        }

        return null;
    }

    public static String getSeed(String seed) {

        seed = seed.toUpperCase();

        if (seed.length() > SEED_LENGTH_MAX)
            seed = seed.substring(0, SEED_LENGTH_MAX);

        seed = seed.replaceAll("[^A-Z9]", "9");

        seed = StringUtils.rightPad(seed, SEED_LENGTH_MAX, '9');

        return seed;
    }
}

